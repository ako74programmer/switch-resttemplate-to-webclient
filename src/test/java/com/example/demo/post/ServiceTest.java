package com.example.demo.post;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.reactive.function.client.WebClient;

class ServiceTest {

    private RestTemplateService restTemplateService = new RestTemplateService(new RestTemplateBuilder());
    private WebClientService webClientService = new WebClientService(WebClient.builder());

    @Test
    void itShouldEqualsFindAll() {
        int restSize = restTemplateService.findAll().size();
        int webSize = webClientService.findAll().size();
        Assertions.assertEquals(restSize, webSize);
    }

    @Test
    void itShouldEqualsFindById() {
        PostDto restPost = restTemplateService.findById(1);
        PostDto webPost = webClientService.findById(1);

        Assertions.assertAll(
                () ->  Assertions.assertNotNull(restPost),
                () ->  Assertions.assertNotNull(webPost),
                () -> Assertions.assertEquals(restPost.body, webPost.body)
        );
    }

    @Test
    void itShouldEqualsCrete() {
        PostDto postDtoRest = new PostDto(1, 101, "title", "body");
        PostDto postRest = restTemplateService.create(postDtoRest);
        PostDto postDtoWeb = new PostDto(1, 102, "title", "body");
        PostDto postWeb = webClientService.create(postDtoWeb);

        Assertions.assertEquals(postRest.body, postWeb.body);
    }

    @Test
    void itShouldDelete() {
        restTemplateService.delete(1);
        webClientService.delete(2);
    }

    @Test
    void itShouldUpdate() {
        PostDto postDto = new PostDto(1, 1, "title", "body");
        restTemplateService.update(postDto);
        webClientService.update(postDto);
    }
}
