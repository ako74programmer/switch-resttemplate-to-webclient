package com.example.demo.post;

import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
public class WebClientService implements PostDao {
    private final WebClient webClient;

    public WebClientService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://jsonplaceholder.typicode.com").build();
    }

//    public Flux<String> findAllTitle() {
//         return findAll()
//                .log()
//                .filter(post -> !post.getTitle().isBlank())
//                .flatMap(post -> Mono.just(post.getTitle().toUpperCase()));
//    }

    @Override
    public List<PostDto> findAll() {
        Flux<PostDto> flux = this.webClient.get()
                .uri("/posts")
                .retrieve()
                .bodyToFlux(PostDto.class);

        List<PostDto> posts = flux.collectList().block();
        return posts;
    }

    @Override
    public PostDto findById(int id) {
        Mono<PostDto> mono = this.webClient.get()
                .uri("/posts/{id}", id)
                .retrieve()
                /*.onStatus(httpStatus -> HttpStatus.NOT_FOUND.equals(httpStatus),
                        clientResponse -> Mono.empty())*/
                .bodyToMono(PostDto.class);

                return mono.blockOptional().orElseThrow();
    }

    @Override
    public PostDto create(PostDto post) {
        Mono<PostDto> mono = webClient.post()
                .uri("/posts")
                .body(Mono.just(post), PostDto.class)
                .retrieve()
                .bodyToMono(PostDto.class);

        return mono.block();
    }

    @Override
    public void delete(int id) {
        webClient.delete()
                .uri("/posts/{id}", id)
                .retrieve()
                .bodyToMono(Void.class);
    }

    @Override
    public void update(PostDto put) {
        webClient.put()
                .uri("/posts/" + put.getId())
                .body(Mono.just(put), PostDto.class)
                .retrieve()
                .bodyToMono(PostDto.class);
    }


}
