package com.example.demo.post;

import java.util.List;

public interface PostDao {
    List<PostDto> findAll();
    PostDto findById(int id);
    PostDto create(PostDto post);
    void delete(int id);
    void update(PostDto put);
}
