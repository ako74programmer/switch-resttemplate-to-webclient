package com.example.demo.post;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class RestTemplateService implements PostDao {
    private static final String BASE_URL = "https://jsonplaceholder.typicode.com";

    private final RestTemplate restTemplate;

    public RestTemplateService(RestTemplateBuilder builder) {
        this.restTemplate = builder
                .rootUri(BASE_URL)
                .build();

    }

    @Override
    public List<PostDto> findAll() {
       return restTemplate.getForObject("/posts", List.class);
    }

    @Override
    public PostDto findById(int id) {
        return restTemplate.getForObject("/posts/{id}", PostDto.class, id);
    }

    @Override
    public PostDto create(PostDto post) {
        return restTemplate.postForObject("/posts", post, PostDto.class);
    }

    @Override
    public void delete(int id) {
        restTemplate.delete("/posts/{id}", id);
    }

    @Override
    public void update(PostDto put) {
        restTemplate.put("/posts/" + put.getId(), put, PostDto.class);
    }

//    public Post getRecipeByTitle(String title) {
//        Map<String, String> requestParameters = new HashMap<>();
//        requestParameters.put("title", title);
//        return restTemplate.getForObject("/recipe", Post.class, requestParameters);
//    }

}
